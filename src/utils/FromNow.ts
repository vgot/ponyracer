import { formatDistanceToNowStrict, parseISO } from 'date-fns';

export default function fromNow(date: string): string {
  const parsedDate = parseISO(date);

  return formatDistanceToNowStrict(parsedDate, { addSuffix: true });
}
