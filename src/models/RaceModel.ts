import { PonyModel } from '@/models/PonyModel';

export interface RaceModel {
  id: number;
  name: string;
  ponies: PonyModel[];
  startInstant: string;
}
